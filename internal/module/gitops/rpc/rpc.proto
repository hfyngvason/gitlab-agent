syntax = "proto3";

// If you make any changes make sure you run: make regenerate-proto

package rpc;

option go_package = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/internal/module/gitops/rpc";

import "pkg/agentcfg/agentcfg.proto";
import "internal/tool/grpctool/automata/automata.proto";

message ObjectsToSynchronizeRequest {
  // Project to fetch Kubernetes object manifests from.
  // e.g. gitlab-org/cluster-integration/gitlab-agent
  string project_id = 1;
  // Last processed commit id. Optional.
  // Server will only send objects if the last commit on the branch is
  // a different one. If a connection breaks, this allows to resume
  // the stream without sending the same data again.
  string commit_id = 2;
  // A list of paths inside of the project to scan
  // for .yaml/.yml/.json manifest files.
  repeated agentcfg.PathCF paths = 3;
}

message ObjectsToSynchronizeResponse {
  // First message of the stream.
  message Headers {
    // Commit id of the manifest repository.
    // Can be used to resume connection from where it dropped.
    string commit_id = 1;
  }
  // Subsequent messages of the stream.
  message Object {
    // Source of the YAML e.g. file name.
    // Several subsequent messages may contain the same source string.
    // That means data should be accumulated to form the whole blob of data.
    string source = 1;

    // YAML object manifest.
    // Might be partial data, see comment for source.
    bytes data = 2;
  }
  // Last message of the stream.
  message Trailers {
  }
  oneof message {

    option (automata.first_allowed_field) = 1;

    Headers headers = 1 [(automata.automata).next_allowed_field = 2, (automata.automata).next_allowed_field = 3];
    Object object = 2 [(automata.automata).next_allowed_field = 2, (automata.automata).next_allowed_field = 3];
    Trailers trailers = 3 [(automata.automata).next_allowed_field = -1];
  }
}

service Gitops {
  // Fetch Kubernetes objects to synchronize with the cluster.
  // Server closes the stream when it's done transmitting the full batch of
  // objects. New request should be made after that to get the next batch.
  rpc GetObjectsToSynchronize (ObjectsToSynchronizeRequest) returns (stream ObjectsToSynchronizeResponse) {
  }
}
